import React from 'react'
import { cx, css } from 'emotion'

const mastfoot = css`
  color: rgba(255, 255, 255, 0.5);
`

export default function Footer () {
  return (
    <footer className={cx(mastfoot, 'mt-auto')}>
      <div className='inner'>
        <p>
          Cover is a backup application
        </p>
      </div>
    </footer>
  )
}
