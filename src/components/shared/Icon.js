import React from 'react'
import { ReactSVG } from 'react-svg'
import {css, cx} from 'emotion'

const iconsPath = 'assets/icons/'

const wrapperCss = cx('icon svg', css`
  vertical-align: bottom;
  display: inline-block;
  margin: 0 0.24em;
`)

export default function Icon ({
  name,
  width = 32,
  height = 32,
  alt = '',
  title = '',
  className = ''
}) {

  return (
    <ReactSVG
      src={`${iconsPath}${name}.svg`}
      className={wrapperCss}
      wrapper='span'
      beforeInjection={svg => {
        // Add a class name to the SVG element.
        svg.classList.add(...className.split(' '))

        // Add inline style to the SVG element.
        svg.setAttribute('style', `width: ${width}px; height: ${height}px`)

        // Modify the first `g` element within the SVG.
        // const [firstGElement] = [...svg.querySelectorAll('g')]
        // firstGElement.setAttribute('fill', 'blue')
      }}
    />
  )
}
