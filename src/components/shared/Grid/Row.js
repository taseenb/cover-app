import React from 'react'

export default React.memo(
  ({
    id = null,
    className = '',
    children,
    'no-gutters': noGutters = false,
    onClick = null
  }) => {
    return (
      <div
        id={id}
        className={`row ${className} ${noGutters ? 'no-gutters' : ''}`}
        onClick={onClick}
      >
        {children}
      </div>
    )
  }
)
