import React from 'react'

export default React.memo(({ id = null, className = '', fluid = false, children }) => {
  return (
    <div id={id} className={`container${fluid ? '-fluid' : ''} ${className}`}>
      {children}
    </div>
  )
})
