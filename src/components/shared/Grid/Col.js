import React from 'react'
import PropTypes from 'prop-types'
// import classnames from 'classnames'
import { cx } from 'emotion'

/**
 * Create a bootstrap column in a 12 columns grid (number from 1 to 12).
 * Options:
 * - auto {boolean} Auto size.
 * - size {number} Mobile first default > 0px (xs).
 * - xs {number} Column size for xs devices (> 0px).
 * - sm {number} Column size for sm devices.
 * - md {number} Column size for md devices.
 * - lg {number} Column size for lg devices.
 * - xl {number} Column size for xl devices.
 * - order {number} Order value.
 * - order-sm {number} Order value for sm devices.
 * - order-md {number} Order value for md devices.
 * - order-lg {number} Order value for lg devices.
 * - order-xl {number} Order value for xl devices.
 * - offset {number} Offset value.
 * - offset-sm {number} Offset value for sm devices.
 * - offset-md {number} Offset value for md devices.
 * - offset-lg {number} Offset value for lg devices.
 * - offset-xl {number} Offset value for xl devices.
 * - newline {boolean} Break to a new line.
 */
const Col = React.memo(
  ({
    id = null,
    onClick = null,
    className = '',
    size,
    children,
    xs,
    sm,
    md,
    lg,
    xl,
    order,
    'order-sm': ordersm,
    'order-md': ordermd,
    'order-lg': orderlg,
    'order-xl': orderxl,
    offset,
    'offset-sm': offsetsm,
    'offset-md': offsetmd,
    'offset-lg': offsetlg,
    'offset-xl': offsetxl,
    auto,
    'auto-sm': autosm,
    'auto-md': automd,
    'auto-lg': autolg,
    'auto-xl': autoxl,
    newline
  }) => {
    let classes
    const original = className || ''

    // Size
    const colSize = size ? 'col-' + size : '' // xs (mobile first default)
    const xsSize = xs ? 'col-xs-' + xs : ''
    const smSize = sm ? 'col-sm-' + sm : ''
    const mdSize = md ? 'col-md-' + md : ''
    const lgSize = lg ? 'col-lg-' + lg : ''
    const xlSize = xl ? 'col-xl-' + xl : ''

    // Order
    const colOrder = order ? 'order-' + order : ''
    const smOrder = ordersm ? 'order-sm-' + ordersm : ''
    const mdOrder = ordermd ? 'order-md-' + ordermd : ''
    const lgOrder = orderlg ? 'order-lg-' + orderlg : ''
    const xlOrder = orderxl ? 'order-xl-' + orderxl : ''

    // Offset
    const colOffset = offset ? 'offset-' + offset : ''
    const smOffset = offsetsm ? 'offset-sm-' + offsetsm : ''
    const mdOffset = offsetmd ? 'offset-md-' + offsetmd : ''
    const lgOffset = offsetlg ? 'offset-lg-' + offsetlg : ''
    const xlOffset = offsetxl ? 'offset-xl-' + offsetxl : ''

    // Auto (variable width content)
    const colAuto = auto ? 'col-auto' : ''
    const smAuto = autosm ? 'col-sm-auto' : ''
    const mdAuto = automd ? 'col-md-auto' : ''
    const lgAuto = autolg ? 'col-lg-auto' : ''
    const xlAuto = autoxl ? 'col-xl-auto' : ''

    if (newline) {
      classes = 'w-100'
    } else {
      classes = [
        'col',
        colSize,
        xsSize,
        smSize,
        mdSize,
        lgSize,
        xlSize,
        colOrder,
        smOrder,
        mdOrder,
        lgOrder,
        xlOrder,
        colOffset,
        smOffset,
        mdOffset,
        lgOffset,
        xlOffset,
        colAuto,
        smAuto,
        mdAuto,
        lgAuto,
        xlAuto
      ]
    }

    return (
      <div id={id} className={cx(original, classes)} onClick={onClick}>
        {children}
      </div>
    )
  }
)

Col.propTypes = {
  className: PropTypes.string,
  size: PropTypes.number,
  xs: PropTypes.number,
  sm: PropTypes.number,
  md: PropTypes.number,
  lg: PropTypes.number,
  xl: PropTypes.number,
  order: PropTypes.number,
  'order-sm': PropTypes.number,
  'order-md': PropTypes.number,
  'order-lg': PropTypes.number,
  'order-xl': PropTypes.number,
  offset: PropTypes.number,
  'offset-sm': PropTypes.number,
  'offset-md': PropTypes.number,
  'offset-lg': PropTypes.number,
  'offset-xl': PropTypes.number,
  auto: PropTypes.bool,
  'auto-sm': PropTypes.bool,
  'auto-md': PropTypes.bool,
  'auto-lg': PropTypes.bool,
  'auto-xl': PropTypes.bool
}

export default Col
