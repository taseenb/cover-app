import React from 'react'
import { css, cx } from 'emotion'

const cover = css`
  max-width: 42em;
`

export default function MainContainer ({ children }) {
  return (
    <div className={cx(cover, 'd-flex w-100 h-100 p-3 mx-auto flex-column')}>
      {children}
    </div>
  )
}
