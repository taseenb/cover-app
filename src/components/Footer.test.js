import React from 'react';
import { render } from '@testing-library/react';
import Footer from './Footer';

test('renders footer text', () => {
  const { getByText } = render(<Footer />);
  const linkElement = getByText(/Cover is a backup application/i);
  expect(linkElement).toBeInTheDocument();
});
