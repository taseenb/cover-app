import React, { useState } from 'react'
// import Icon from './shared/Icon'
import { css, cx } from 'emotion'
import { useSpring, animated } from 'react-spring'

import S3 from './S3'

const cover = css`
  padding: 0 1.5rem;
  text-align: center;
`
const btnLg = css`
  padding: 0.75rem 1.25rem;
  font-weight: 700;
`

export default function Main () {
  const [showButton, setShowButton] = useState(true)
  const { opacity, y } = useSpring({
    opacity: showButton ? 1 : 0,
    y: showButton ? 0 : 30,
    from: { opacity: 0, y: 30 }
  })

  function anim () {
    setShowButton(!showButton)
  }

  return (
    <main role='main' className={cx(cover, 'inner')} onClick={anim}>
      <h1 className='cover-heading'>
        {/* <Icon name='alarm' className='text-success' /> */}
        Cover your page.
      </h1>
      <p className='lead'>
        Cover is a one-page template for building simple and beautiful home
        pages. Download, edit the text, and add your own fullscreen background
        photo to make it your own.
      </p>
      <animated.p
        className='lead'
        style={{
          opacity: opacity.interpolate(o => o),
          transform: y.interpolate(y => `translate3d(0, ${y}px, 0)`)
        }}
      >
        <button className={cx(btnLg, 'btn btn-lg btn-secondary')}>
          Learn more!
        </button>
      </animated.p>
    </main>
  )
}
