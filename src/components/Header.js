import React from 'react'
import { css, cx } from 'emotion'

const masthead = css`
  margin-bottom: 2rem;
`

const navMasthead = css`
  @media (min-width: 48em) {
    float: right;
  }
`

const mastheadBrand = css`
  margin-bottom: 0;
  @media (min-width: 48em) {
    float: left;
  }
`

const navLink = css`
  padding: 0.25rem 0;
  font-weight: 700;
  color: rgba(255, 255, 255, 0.5);
  background-color: transparent;
  border-bottom: 0.25rem solid transparent;
  margin-left: 1rem;
  &:hover,
  &:focus {
    border-bottom-color: rgba(255, 255, 255, 0.25);
    text-decoration: none;
  }
`

const active = css`
  color: #fff;
  border-bottom-color: #fff;
`

export default function Header () {
  return (
    <header className={cx(masthead, 'mb-auto')}>
      <div className='inner'>
        <h3 className={mastheadBrand}>Cover</h3>
        <nav className={cx(navMasthead, 'nav justify-content-center')}>
          <a className={cx(navLink, active)} href='/backups'>
            Backups
          </a>
          <a className={cx(navLink)} href='/settings'>
            Settings
          </a>
          <a className={cx(navLink)} href='/account'>
            Account
          </a>
        </nav>
      </div>
    </header>
  )
}
