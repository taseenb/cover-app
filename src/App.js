import './styles/main.sass'

import React from 'react'
import Header from './components/Header'
import Footer from './components/Footer'
import Main from './components/Main'
import MainContainer from './components/MainContainer'

// const os = require('os')
// Platforms: darwin', 'linux', 'win32'
// https://nodejs.org/docs/latest-v13.x/api/os.html#os_os_platform
// console.log('OS platform', os.platform())
// console.log('OS arch', os.arch())
// console.log('OS type', os.type())
// console.log('OS release', os.release())

function App () {
  return (
    <MainContainer>
      <Header />
      <Main />
      <Footer />
    </MainContainer>
  )
}

export default App
