import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'

const {remote, ipcRenderer} = window.require('electron')
const fs = remote.require('fs')
const os = remote.require('os')
const isDev = remote.require('electron-is-dev')

console.log(isDev)

// Platforms: darwin', 'linux', 'win32'
// https://nodejs.org/docs/latest-v13.x/api/os.html#os_os_platform
console.log('OS platform?', os.platform())

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
)