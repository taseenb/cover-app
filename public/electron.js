const os = require('os')
const { app, BrowserWindow, session, shell } = require('electron')
const path = require('path')
const isDev = require('electron-is-dev')

let mainWindow

// Platforms: darwin', 'linux', 'win32'
// https://nodejs.org/docs/latest-v13.x/api/os.html#os_os_platform
console.log('OS platform', os.platform())

function createWindow () {
  mainWindow = new BrowserWindow({
    width: 900,
    height: 680,
    backgroundColor: '#fff',
    webPreferences: {
      devTools: isDev,
      enableRemoteModule: true,
      nodeIntegration: true
    }
  })

  const devUrl = isDev
    ? 'http://localhost:3000'
    : `file://${path.join(__dirname, '../build/index.html')}`

  mainWindow.loadURL(devUrl)

  if (isDev) {
    // Open the DevTools.
    // BrowserWindow.addDevToolsExtension('<location to your react chrome extension>')
    mainWindow.webContents.openDevTools()

    const contextMenu = require('electron-context-menu')

    contextMenu({
      prepend: (defaultActions, params, browserWindow) => [
        {
          label: 'Rainbow',
          // Only show it when right-clicking images
          visible: params.mediaType === 'image'
        },
        {
          label: 'Search DuckDuckGo for “{selection}”',
          // Only show it when right-clicking text
          visible: params.selectionText.trim().length > 0,
          click: () => {
            shell.openExternal(
              `https://duckduckgo.com/?q=${encodeURIComponent(
                params.selectionText
              )}`
            )
          }
        }
      ]
    })
  }

  mainWindow.setMenu(null)
  mainWindow.on('closed', () => (mainWindow = null))
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

// Do not allow external navigation
app.on('web-contents-created', (event, contents) => {
  contents.on('will-navigate', (event, navigationUrl) => {
    const parsedUrl = new URL(navigationUrl)

    if (parsedUrl.origin !== 'http://localhost') {
      event.preventDefault()
    }
  })
})

// Allow notifications in Windows
app.setAppUserModelId(process.execPath)
